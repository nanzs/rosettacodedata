/*REXX pgm demonstrates move-to-front algorithm encode/decode sym table.*/
parse arg stuff;    if stuff=''  then stuff='broood bananaaa hiphophiphop'
  do j=1  for words(stuff);   x=word(stuff,j) /*process 1 word at a time*/
  abc='abcdefghijklmnopqrstuvwxyz';   $=      /*define the symbol table.*/
      do k=1  for length(x);  z=substr(x,k,1) /*encrypt a letter in word*/
      _=pos(z,abc);  if _==0  then iterate    /*pos of letter in sym tab*/
      $=$ _-1;   abc=z || delstr(abc,_,1)     /*adjust the symbol table.*/
      end   /*k*/                             /* [↑]  move-to-front alg.*/
  abc='abcdefghijklmnopqrstuvwxyz';   !=      /*define the symbol table.*/
      do m=1  for words($);     n=word($,m)+1 /*decode the sequence tab.*/
      y=substr(abc,n,1);        !=!||y        /*decode a letter of word.*/
      abc=y || delstr(abc,n,1)                /*re-build the symbol tab.*/
      end   /*m*/                             /* [↑]  show word, enc, OK*/
  say 'word: ' left(x,20) 'encoding:' left($,35) word('wrong OK',1+(!==x))
  end  /*j*/                           /*done encoding/decoding words.  */
                                       /*stick a fork in it, we're done.*/
