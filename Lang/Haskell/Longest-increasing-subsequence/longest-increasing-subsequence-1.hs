import Data.Ord
import Data.List
import Data.List.Ordered

--    longest                        increasing
lis = maximumBy (comparing length) . filter isSorted . subsequences

main = do
	print $ lis [3,2,6,4,5,1]
	print $ lis [0,8,4,12,2,10,6,14,1,9,5,13,3,11,7,15]
