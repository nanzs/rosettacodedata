import std.range, std.algorithm, std.array;

void main() {
    char[26] arr = 26
                   .iota
                   .map!(i => cast(char)('a' + i))
                   .array;
}
