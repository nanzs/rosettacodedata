constant number-of-decimals = 100;

multi Sqrt(Int $n) {
    .[*-1] given
    1, { ($_ + $n div $_) div 2 } ... * == *
}
multi Sqrt(FatRat $r --> FatRat) {
    return FatRat.new:
    Sqrt($r.nude[0] * 10**(number-of-decimals*2) div $r.nude[1]),
    10**number-of-decimals;
}

my FatRat ($a, $n) = 1.FatRat xx 2;
my FatRat $g = Sqrt(1/2.FatRat);
my $z = .25;

for ^10 {
    given [ ($a + $g)/2, Sqrt($a * $g) ] {
	$z -= (.[0] - $a)**2 * $n;
	$n += $n;
	($a, $g) = @$_;
	say ~.match: EVAL("/ .* '.' .**{number-of-decimals} /")
	given $a ** 2 / $z;
    }
}
