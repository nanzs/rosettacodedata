constant ln2 = [+] map { 1.FatRat / 2**$_ / $_ }, 1 .. 100;
constant fact = 1, [\*] 1..*;

constant h = fact Z/ (2*ln2, { $^prev * ln2 } ... *);

use Test;
plan *;

for h[1..17] {
    ok m/'.'<[09]>/, .round(0.001)
}
