def partition(mask)
  return [[]] if mask.empty?
  [*1..mask.inject(:+)].permutation.map {|perm|
    mask.map {|num_elts| perm.shift(num_elts).sort }
  }.uniq
end

[[],[0,0,0],[1,1,1],[2,0,2]].each do |test_case|
  puts "partitions #{test_case}:"
  partition(test_case).each{|part| p part }
  puts
end
