public struct TinyInt
{
    private const int minimalValue = 1;
    private const int maximalValue = 10;

    private readonly int value;

    private TinyInt(int i)
    {
        if (minimalValue > i || i > maximalValue)
        {
            throw new System.ArgumentOutOfRangeException();
        }
        value = i;
    }

    public static implicit operator int(TinyInt i)
    {
        return i.value;
    }

    public static implicit operator TinyInt(int i)
    {
        return new TinyInt(i);
    }
}
