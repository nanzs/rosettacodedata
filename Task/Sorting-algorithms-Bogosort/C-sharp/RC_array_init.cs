using System;
using System.Collections.Generic;

class Program {
      static void Main() {
      	     List<int> a = new List<int>();
 	     RC_array_init(a, 1000000, 100);
//      	     foreach (int i in a) Console.Write(i + "\n");
      }

static void RC_array_init(List<int> array, int num, int seed) {
  const int LCG_A = 1664525, LCG_C = 1013904223;
  for (int j = 0; j < num; j++) {
    seed = (LCG_A * seed + LCG_C) & 0x7fffffff;
    array.Add(seed);
  }
}

}