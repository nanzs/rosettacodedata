LCG_A = 1664525
LCG_C = 1013904223
def RC_array_init(num, seed)
  a = []
  for j in 0..num-1
    seed = (LCG_A * seed + LCG_C) & 0x7fff_ffff
    a << seed
  end
  return a
end

a = RC_array_init(1000000, 100)
#puts a
