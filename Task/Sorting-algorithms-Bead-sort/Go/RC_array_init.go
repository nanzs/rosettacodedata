package main
 
import "fmt"
 
func lcg(num int, seed uint32) []int {
     LCG_A := uint32(1664525)
     LCG_C := uint32(1013904223)
     array := make([]int, num)
     for j := 0; j < num; j++ {
     	 seed = (LCG_A*seed + LCG_C) & 0x7fffffff
	 array[j] = int(seed)
     }
     return array
}

func main() {
     a := lcg(1000000, 100)
     for j := 0; j < 1000000; j++ {
     	 fmt.Println(a[j])
     }
}