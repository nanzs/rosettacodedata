(defn rot-13' [^Character c]
  (condp #(re-find %1 (str %2)) c
    #"[A-Ma-m]" (-> c int (+ 13) char)
    #"[N-Zn-z]" (-> c int (- 13) char)
    c))

; Elapsed time: 0.320552 msecs
(apply str (map rot-13' "The Quick Brown Fox Jumped Over The Lazy Dog!"))
