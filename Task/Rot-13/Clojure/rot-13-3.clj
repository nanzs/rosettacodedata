(let [A (into #{} "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
      Am (->> (cycle A) (drop 26) (take 52) (zipmap A))]
  (defn rot13 [^String in]
    (apply str (map #(Am % %) in))))

; Elapsed time: 0.222424 msecs
(rot13 "The Quick Brown Fox Jumped Over The Lazy Dog!")
