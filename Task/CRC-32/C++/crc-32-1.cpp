#include <string>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <array>
#include <cstdint>

class CRC32
{
public:
    CRC32()
    {
        generateTable();
    }

    template<class InputIterator>
    std::uint_fast32_t get( InputIterator begin, InputIterator end )
    {
        return ~std::accumulate( begin, end, ~std::uint_fast32_t{0},
            [this](std::uint_fast32_t nCRC, std::uint_fast8_t nVal)
                { return m_pTable[ (nCRC ^ nVal) & 0xFF] ^ (nCRC >> 8); } );
    }

private:
    void generateTable()
    {
        // fill the table with 0..255
        std::iota( m_pTable.begin(), m_pTable.end(), std::uint_fast32_t{0} );

        // calculate the crc table
        for (int j = 0; j < 8; j++)
        {
            std::transform( m_pTable.begin(), m_pTable.end(), m_pTable.begin(),
                [] ( std::uint_fast32_t &nValue ) { return (nValue>>1)^((nValue&1)*0xedb88320); } );
        }
    }

private:
    std::array<std::uint_fast32_t, 256> m_pTable;
};

int main()
{
    CRC32 oCrc;
    std::string str( "The quick brown fox jumps over the lazy dog" );
    std::cout << "Checksum: " << std::hex << oCrc.get( str.begin(), str.end() ) << std::endl;
    return 0;
}
