using System;

class Program
{
    delegate Func<A,C> Composer<A,B,C>(Func<B,C> f, Func<A,B> g);

    static void Main(string[] args)
    {
        Func<double, double> cube = x => Math.Pow(x, 3);
        Func<double, double> croot = x => Math.Pow(x, (double)1/3);

        var fun = new[] { Math.Sin, Math.Cos, cube };
        var inv = new[] { Math.Asin, Math.Acos, croot };
        Composer<double, double, double> compose = (f, g) => delegate(double x) { return f(g(x)); };

        for (var i = 0; i < fun.Length; ++i)
        {
            Console.WriteLine(compose(fun[i],inv[i])(0.5));
        }
    }
}
