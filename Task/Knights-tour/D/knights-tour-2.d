import std.stdio, std.math, std.algorithm, std.range;

alias Sq = Tuple!(int,"x", int,"y"); /// Square.

const(Sq[]) knightTour(in Sq[] board, in Sq[] moves) /*pure nothrow*/ {
    enum findMoves = (in Sq sq) /*nothrow*/ =>
        cartesianProduct([1, -1, 2, -2], [1, -1, 2, -2])
        .filter!(ij => ij[0].abs != ij[1].abs)
        .map!(ij => Sq(sq.x + ij[0], sq.y + ij[1]))
        .filter!(s => board.canFind(s) && !moves.canFind(s));
    auto newMoves = findMoves(moves.back);
    if (newMoves.empty) return moves;
    //const newSq = newMoves.reduce!(min!(m=>findMoves(m).walkLength));
    immutable newSq = newMoves
                      .map!(s => tuple(findMoves(s).walkLength, s))
                      .reduce!min[1];
    return board.knightTour(moves ~ newSq);
}

void main(in string[] args) {
    enum toSq = (in string xy) => Sq(xy[0] - '`', xy[1] - '0');
    immutable toAlg = (in Sq s) => [dchar(s.x + '`'),dchar(s.y + '0')];
    immutable sq = toSq((args.length == 2) ? args[1] : "e5");
    const board = iota(1, 9).cartesianProduct(iota(1, 9)).map!Sq.array;
    writefln("%(%-(%s -> %)\n%)",
             board.knightTour([sq]).map!toAlg.chunks(8));
}
