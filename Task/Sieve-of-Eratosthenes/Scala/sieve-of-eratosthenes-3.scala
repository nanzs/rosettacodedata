import scala.collection.mutable.ArrayBuffer
  import scala.annotation.tailrec

  def SoEPg(): Iterator[Int] = {
    val basePrimesArr: ArrayBuffer[Int] = ArrayBuffer.empty

    def makePg(lowp: Int, basePrimes: Iterator[Int]) = {
      val nxtlowp = lowp + 262144
      val low = (lowp - 3) >>> 1
      val buf: Array[Int] = new Array(131072)

      def nextPrime(c: Int) = {
        @tailrec def nexti(i: Int): Int = {
          if ((i < 131072) && ((buf(i >>> 5) & (1 << (i & 31)))) != 0) nexti(i + 1)
          else i
        }
        (nexti((c - lowp) >>> 1) << 1) + lowp
      }

      def cullp(s: Int, p: Int) = {
        @tailrec def cull(c: Int): Unit = {
          if (c < 131072) {
            buf(c >>> 5) |= 1 << (c & 31)
            cull(c + p)
          }
        }
        cull(s)
      }

      val bps = if (low <= 0) { //page zero has no base primes...
        Iterator.iterate(3)(p => nextPrime(p + 2))
          .takeWhile(_ <= Math.sqrt(262145).toInt).foreach(p => cullp((p * p - 3) >>> 1, p))
        basePrimes
      } else {
        val nbps = if (basePrimesArr.length == 0) {
          val nbasePrimes = SoEPg()
          nbasePrimes.next() // new source primes > 2
          basePrimesArr += nbasePrimes.next()
          nbasePrimes
        } else basePrimes

        def fillBasePrimes(bp: Int): Int = {
          if (bp * bp < nxtlowp) {
            val np = nbps.next(); basePrimesArr += np
            fillBasePrimes(np)
          } else bp
        }

        fillBasePrimes(basePrimesArr(basePrimesArr.length - 1))
        for (p <- basePrimesArr) {
          val b = (p * p - 3) >>> 1
          val s = if (b >= low) b - low else { val r = (low - b) % p; if (r != 0) p - r else 0 }
          cullp(s, p)
        }
        nbps
      }
      (Iterator.iterate(nextPrime(lowp))(p => nextPrime(p + 2)).takeWhile(_ < nxtlowp), nxtlowp, bps)
    }

    Iterator.single(2) ++
      Iterator.iterate(makePg(3, Iterator.empty)) { case (_, nlp, b) => makePg(nlp, b) }.
      map{ case (itr, _, _) => itr }.flatten
  }
