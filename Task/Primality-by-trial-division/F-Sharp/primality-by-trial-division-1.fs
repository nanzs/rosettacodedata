open NUnit.Framework
open FsUnit

let isPrime x =
  match x with
  | 2 | 3 -> true
  | x when x % 2 = 0 -> false
  | _ ->
     let rec aux i =
        match i with
        | i when x % i = 0 -> false
        | i when x < i*i -> true
        | _ -> aux (i+2)
     aux 3

[<Test>]
let ``Validate that 2 is prime`` () =
  isPrime 2 |> should equal true

[<Test>]
let ``Validate that 4 is not prime`` () =
  isPrime 4 |> should equal false

[<Test>]
let ``Validate that 3 is prime`` () =
  isPrime 3 |> should equal true

[<Test>]
let ``Validate that 9 is not prime`` () =
  isPrime 9 |> should equal false

[<Test>]
let ``Validate that 5 is prime`` () =
  isPrime 5 |> should equal true

[<Test>]
let ``Validate that 277 is prime`` () =
  isPrime 277 |> should equal true
