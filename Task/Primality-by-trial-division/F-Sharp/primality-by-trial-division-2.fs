let isPrime x =
  if x < 2 then false else
  if x = 2 then true else
  if x % 2 = 0 then false else
  seq { 3 .. 2 .. int(sqrt (double x)) } |> Seq.forall (fun i -> x % i <> 0)
