public static int Lcm(int m, int n)
    {
      int r = 0;
      Func<int, int, int> gcd = delegate(int m2, int n2)
                                  {
                                    while (n2!=0)
                                    {
                                      var t2 = m2;
                                      m2 = n2;
                                      n2 = t2%n2;
                                    }
                                    return m2;
                                  };

      try
      {
        if (m == 0 || n == 0)
          throw new ArgumentException();
        r = Math.Abs(m*n)/gcd(m, n);
      }
      catch(Exception exception)
      {
        Console.WriteLine(exception.Message);
      }
      return (r<0) ? -r : r;
    }
