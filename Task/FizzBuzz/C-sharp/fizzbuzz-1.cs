using System;
using System.Text;

namespace FizzBuzz
{
    class Program
    {
        static void Main()
        {
            var output = new StringBuilder();
            for (var i = 1; i <= 100; i++)
            {
                output.AppendFormat("{0}{1}{2}",
                    (i%3 == 0) ? "Fizz" : string.Empty,
                    (i%5 == 0) ? "Buzz" : string.Empty,
                    Environment.NewLine);
            }
            Console.WriteLine(output);
        }
    }
}
