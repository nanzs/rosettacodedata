using System;
using System.Linq;

namespace CommaQuibbling
{
    internal static class Program
    {
        #region Static Members

        private static string Quibble( string[] input )
        {
            var len = input.Length;
            return
                "{" +
                ( len > 1 ? ( String.Join( ", ", input.Take( len - 1 ) ) + " and " ) : ( "" ) ) +
                ( input.LastOrDefault() ?? "" ) +
                "}";
        }

        private static void Main()
        {
            Console.WriteLine( Quibble( new string[] {} ) );
            Console.WriteLine( Quibble( new[] {"ABC"} ) );
            Console.WriteLine( Quibble( new[] {"ABC", "DEF"} ) );
            Console.WriteLine( Quibble( new[] {"ABC", "DEF", "G", "H"} ) );

            Console.WriteLine( "< Press Any Key >" );
            Console.ReadKey();
        }

        #endregion
    }
}
