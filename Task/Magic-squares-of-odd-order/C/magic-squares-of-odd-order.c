#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
#       define M(x) ((x + n - 1) % n)
        int i, j, k, n, *m;
        char fmt[16];

        if (argc < 2 || (n = atoi(argv[1])) <= 0 || !(n&1))
                fprintf(stderr, "forcing size %d\n", n = 5);

        m = calloc(n*n, sizeof(*m));

        i = 0, j = n/2;
        for (k = 1; k <= n*n; k++) {
                m[i*n + j] = k;
                if (m[M(i)*n + M(j)])
                        i = (i+1) % n;
                else
                        i = M(i), j = M(j);
        }

        for (i = 2, j = 1; j <= n*n; ++i, j *= 10);
        sprintf(fmt, "%%%dd", i);

        for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++)
                        printf(fmt, m[i*n + j]);
                putchar('\n');
        }

        return 0;
}
