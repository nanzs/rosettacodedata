/*REXX pgm generates all random starting positions for the Chess960 game*/
parse arg seed .                       /*allow for (RAND) repeatability.*/
if seed\==''  then call random ,,seed  /*if SEED specified, use the seed*/
x.=0;  #=0

do t=1 /*═══════════════════════════════════════════════════════════════*/
if t//1000==0  then say right(t,9) 'random generations: '  #  " unique starting positions."
@.=.                                   /*define the (empty) first rank. */
r1=random(1,6);       @.r1='R'         /*place the  first rook on rank1.*/
r1m=r1-1;             r1p=r1+1         /*used for faster comparisons.   */
   do forever;  r2=random(1,8)         /*try to get a random second rook*/
   if r2==r1    then iterate           /*position is the same as rook1. */
   if r2==r1m   then iterate           /*it's immediately before rook1. */
   if r2==r1p   then iterate           /*  "       "       after   "    */
   leave                               /*found a good 2nd rook placement*/
   end   /*forever*/                   /* [↑]  separate IFs for speed.  */
@.r2='r'                               /*place the second rook on rank1.*/
c1=min(r1,r2);  c2=max(r1,r2)          /*order the two rooks for RANDOM.*/
_ =random(c1+1,c2-1);    @._ ='K'      /*  "    "   only  king  "   "   */
          do _=0      ; b1=random(1,8);  if @.b1\==. then iterate; c=b1//2
            do forever; b2=random(1,8) /* c=color of bishop ───────┘    */
            if b2//2==c  then iterate  /*bishop2 = same color as bishop1*/
            if @.b2\==.  then iterate  /*the position is already taken. */
            if b2==b1    then iterate  /*position is taken by bishop 1. */
            leave _                    /*we found a position for bishop2*/
            end   /*forever*/          /* [↑]  find a place: 1st bishop.*/
          end     /*_*/                /* [↑]    "  "   "    2nd    "   */
@.b1='B'                               /*place the  1st  bishop on rank1*/
@.b2='b'                               /*  "    "   2nd     "    "   "  */
                                       /*place two knights on rank1.    */
   do  until @._='N'; _=random(1,8); if @._\==. then iterate; @._='N'; end
   do  until @.!='n'; !=random(1,8); if @.!\==. then iterate; @.!='n'; end
_=                                     /*only the queen is left to place*/
   do i=1  for 8;  _=_ || @.i;   end   /*construct output:  first rank. */
upper _                                /*uppercase all chess pieces.    */
if x._  then iterate                   /*was this position found before?*/
x._=1                                  /*define this position as found. */
#=#+1                                  /*bump the unique positions found*/
if #==960  then leave
end   /*t ══════════════════════════════════════════════════════════════*/

say # 'unique starting positions found after '   t   "generations."
                                       /*stick a fork in it, we're done.*/
