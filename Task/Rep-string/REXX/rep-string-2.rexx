/*REXX pgm determines if a string is a repString, returns min len repStr*/
s=1001110011 1110111011 0010010010 1010101010 1111111111 0100101101 0100100 101 11 00 1

     do k=1  for words(s); _=word(s,k) /*process all the binary strings.*/
     say  right(_,20)  rep_string(_)   /*show the original & the result.*/
     end   /*k*/
exit                                   /*stick a fork in it, we're done.*/
/*───────────────────────────────────REP_STRING subroutine──────────────*/
rep_string:  procedure;  parse arg x;  L=length(x);     r_='  rep string='
   do j=1  for L-1;    p=left(x,j);    if j>L%2  then iterate
   if left(copies(p,L*L),L)==x  then   return r_ left(p,15) '(length' j")"
   end   /*j*/
return  '    (no repetitions)'         /*(sigh)∙∙∙ a failure to find rep*/
