using System;

namespace ConsoleApplication7
{
    class Program
    {
        public static void Main(string[] args)
        {
                int[] array;
                int needle;
                .....
                .....

                int index = binarySearch(array, needle, 0, array.Length);
                Console.WriteLine(needle + ((index == -1) ? " is not in the array" : (" is at index " + index)));
        }


        public static int binarySearch(int[] nums, int check, int lo, int hi){
                if(hi < lo){
                        return -1; //impossible index for "not found"
                }
                int guess = (hi + lo) / 2;
                if(nums[guess] > check){
                        return binarySearch(nums, check, lo, guess - 1);
                }else if(nums[guess]<check){
                        return binarySearch(nums, check, guess + 1, hi);
                }
                return guess;

                }
    }
}
