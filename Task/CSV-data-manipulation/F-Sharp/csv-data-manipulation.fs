open System
open System.Collections.Generic
open System.IO

type Csv = class
    val private _data : Dictionary<int * int, string>
    val mutable private _rows : int
    val mutable private _cols : int

    new () =
        { _data = new Dictionary<int * int, string>();
            _rows = 0; _cols = 0 }

    member this.Rows with get () = this._rows
    member this.Cols with get () = this._cols

    member this.Item
        with get(row,col) =
            match this._data.TryGetValue((row,col)) with
            | (true, s) -> s | _ -> ""
        and  set(row,col) value =
            this._data.[(row,col)] <- value
            this._rows <- Math.Max(this._rows, row + 1)
            this._cols <- Math.Max(this._cols, col + 1)

    member this.Open(stream : StreamReader, delim : char) =
        this._data.Clear()
        this._cols <- 0
        this._rows <- 0

        Seq.unfold (
            fun (s : StreamReader) -> if s.EndOfStream then None else Some(s.ReadLine(), s)) stream
        |> Seq.iteri (
            fun i line ->
                line.Split(delim)
                |> Array.iteri (fun j item -> this.[i,j] <- item))
        stream.Close()

    member this.Open(stream : StreamReader) =
        this.Open(stream, ',')

    member this.Save(writer : TextWriter, delim : char) =
        for row = 0 to this._rows - 1  do
            String.Join(delim.ToString(),
                [0 .. this._cols - 1]
                |> List.map(fun col -> this.[row,col])
                |> List.toArray)
            |> writer.WriteLine

    member this.Save(writer : TextWriter) =
        this.Save(writer, ',')
end


[<EntryPoint>]
let main argv =
    let csv = new Csv()
    csv.Open(new StreamReader(argv.[0]))
    csv.[0, 0] <- "Column0"
    csv.[1, 1] <- "100"
    csv.[2, 2] <- "200"
    csv.[3, 3] <- "300"
    csv.[4, 4] <- "400"
    csv.Save(Console.Out)
    0
