using System.Net.Sockets;

class Program
{
	static void Main(string[] args)
	{
		TcpClient client;

		// Connect
		do
		{
			client = new TcpClient();
			client.Connect(new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 8000));

			Console.WriteLine("Connected");

			// Send
			byte[] tSend = Encoding.ASCII.GetBytes("Hello World!");
			client.GetStream().Write(tSend, 0, tSend.Length);
			client.GetStream().WriteByte(0);

			Console.WriteLine("Sent: " + Encoding.ASCII.GetString(tSend));

			// Read
			string tRecieve = "";
			char t;
			do
			{
				if (client.Available > 0)
				{
					t = (char)client.GetStream().ReadByte();

					if (t == 0)
						break;

					tRecieve += t;
				}
			} while (true);

			Console.WriteLine("Recieved: " + tRecieve);

			client.Close();

			Console.Read();
		} while (true);
	}
}
