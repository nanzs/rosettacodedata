int fib(int arg) pure @nogc {
    assert(arg >= 0);

    return function int (int n) pure nothrow @nogc {
        auto self = __traits(parent, {});
        return (n < 2) ? n : self(n - 1) + self(n - 2);
    }(arg);
}

void main() {
    import std.stdio;

    39.fib.writeln;
}
