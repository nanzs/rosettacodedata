using System;
using System.Collections.Generic;

namespace RosettaCode.MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> testList = new List<int> { 1, 5, 2, 7, 3, 9, 4, 6 };
            printList(testList);
            printList(MergeSorter.Sort(testList));
        }

        private static void printList<T>(List<T> list)
        {
            foreach (var t in list)
            {
                Console.Write(t + " ");
            }
            Console.WriteLine();
        }
    }
}
