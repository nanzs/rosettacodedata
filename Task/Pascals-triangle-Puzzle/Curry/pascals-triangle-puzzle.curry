import CLPFD
import Constraint (allC, andC)
import Findall (findall)
import List (init, last)


solve :: [[Int]] -> (Int,Int,Int) -> Success
solve body@([n]:_) xyz =
    domain (concat body) 1 n
  & go body
  & labeling [] (concat body)
  where
    go (x:ys@(y:_))  = x `atop` y & go ys
    go [[x,_,y,_,z]] = y =# x +# z & (x,y,z) =:= xyz

    x `atop` y = andC $ zipWith (=#) x (init y <+> tail y)
    (<+>) = zipWith (+#)


test = [ [151]
       , [ _,  _]
       , [40,  _, _]
       , [ _,  _, _, _]
       , [ _, 11, _, 4, _]
       ]
main = findall $ solve test
