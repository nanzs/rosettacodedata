## A Comparative Analysis of Programming Languages in Rosetta Code 

This repository contains a snapshot of RosettaCode
(http://rosettacode.org/wiki/Rosetta_Code) together with meta-data and
scripts to run the examples in 8 languages (C, C#, F#, Go, Haskell,
Python, Java, and Ruby). The analysis is described in detail in the
paper: 

   http://arxiv.org/abs/1409.0252


### Analysis scripts

The README file in "analysis" describes the scripts and how to use
them.


### RosettaCode snapshot

File "snapshot.md" in this directory describes how the snapshot of
RosettaCode was generated using a modified version of the Perl module
RosettaCode-0.0.5, available from http://cpan.org/.


## Authors

C. A. Furia <c.a.furia@gmail.com> and S. Nanz <sebastian.nanz@gmail.com>
