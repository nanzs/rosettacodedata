#!/bin/bash
SCRIPTDIR="$( dirname "${BASH_SOURCE[0]}" )"

source "$SCRIPTDIR/preamble"

# compiler and language
OPERATION='compile'
COMPBIN=gcc
OPTIMIZATION='-O2'
LANGEXT=(c h)
std="default"

# dialects of C (gnu90 is the default, and hence not tried as fallback)
DIALECTS=(c90 iso9899:199409 c1x c99 gnu99 gnu1x)

source "$SCRIPTDIR/read_args"

source "$SCRIPTDIR/source_setup"

COMPILESTR="$COMPBIN $OPTIMIZATION -o $target $newinfiles"
if [ ! -z ${CMPOPTS+x} ]; then
	 COMPILESTR="$COMPILESTR ${CMPOPTS[@]}"
fi

source "$SCRIPTDIR/compilation"

$TIMEBIN -o "$log" -f "$fstr" sh -c "$COMPILESTR" > "$stdout" 2> "$stderr"

# check if compilation succeeded
ec=`awk '/exit:/{print $2}' "$log"`
if [ "$ec" -ne 0 ]; then
	 for std in ${DIALECTS[@]}; do
		  COMPILESTR="$COMPBIN -std=$std $OPTIMIZATION -o $target $newinfiles"
		  if [ ! -z ${CMPOPTS+x} ]; then
				COMPILESTR="$COMPILESTR ${CMPOPTS[@]}"
		  fi
		  $TIMEBIN -o "$log" -f "$fstr" sh -c "$COMPILESTR" > "$stdout" 2> "$stderr"
		  ec=`awk '/exit:/{print $2}' "$log"`
		  if [ "$ec" -eq 0 ]; then
				break
		  fi
	 done
fi
add_logentry "std:$std"

if [ -f "$target" ]; then
	 size=`$DUBIN -c "$target" | $AWKBIN '/total$/{print $1}'`
else
	 size=-1
fi
add_logentry "size:$size"

mainfound='yes'
if [ -f "$stderr" ]; then
	 $EGREPBIN -q "undefined reference to \`main'$" "$stderr"
	 if [ "$?" -eq 0 ]; then
		  mainfound='no'
	 fi
fi
add_logentry "mainfound:$mainfound"

source "$SCRIPTDIR/logging"
