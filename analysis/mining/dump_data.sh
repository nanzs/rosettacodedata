#! /bin/bash

LANGUAGES=(C Java Python Haskell Go 'C sharp' 'F Sharp' Ruby)
LANGUAGE_SCRIPT_NAMES=(C Java Python Haskell Go C-sharp F-Sharp Ruby)
OPERATIONS=(compile make run loc)
DATADIR="../data"

for ((nlang = 0; nlang < ${#LANGUAGES[@]}; nlang++)) do
	 lang=${LANGUAGES[$nlang]}
	 script_lang=${LANGUAGE_SCRIPT_NAMES[$nlang]}
	 for op in "${OPERATIONS[@]}"; do
		  datfile="$DATADIR/$op""_""$script_lang.dat"
		  if [ -f "$datfile" ]; then
				./champ.py --language "$lang" --aggregate --serialize "$datfile" "$op" 2> "$DATADIR/anomalies.$op""_""$script_lang.log" | sed '1 s/^## //' > "$DATADIR/$op""_""$script_lang.dsv"
		  fi
	 done
done

./dump_tasks.py > "$DATADIR/tasks.dsv"

sed 's/,x/,TRUE/g' "$DATADIR/todo.csv" | tail -n +8 > "$DATADIR/todo.dsv"
